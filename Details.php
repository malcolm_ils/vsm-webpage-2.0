<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Details
 *
 * @author Malcolm Adams
 */    
require_once("rdb/rdb.php");    
include "data.class.php";
$commsData = new vsm20data();
$client = (int)$_POST['client'];
$site = (int)$_POST['site'];
$dvr = (int)$_POST['dvr'];
    
$conn = r\connect('ilscomms.ilsny.com');
    
class Details {
   // public $conn;
    public $networkDetails;
    public $driveCount;
    public $driveDetails;

    
    function getCheckin($client, $site, $dvr){
        $conn = r\connect('ilscomms.ilsny.com');
        $allclient = $commsData->GetAllClients();
        $result = r\db("VSM2")->table("CheckIn")->filter(array("Client"=> $client,"Site" => $site,"DvrID" => $dvr))->run($conn);
        $rdata = $result ->toArray();
        $rdata = (array)$rdata[0];
        $conn->close();
        return $rdata;
    }
    
    function getSystemDetails($client, $site, $dvr){
        $conn = r\connect('ilscomms.ilsny.com');
        $result = r\db("VSM2")->table("SystemDetails")->filter(array("Client"=> $client,"Site" => $site,"DvrID" => $dvr))->
        pluck(array("NetworkDetails","DriveCount", "DriveDetails"))->run($conn);
        $result  = $result->toArray();
        $rdata= (array)$rdata[0];
        $conn->close();
        return $rdata;
    }
      
    function getTodaysLogs($dvr){
        $date = getDate();
        $today = $date["mday"];
        $month = $date["month"];
        $year = $date["year"];
        $timestamp = strtotime("".$today." ".$month." ".$year."");  
        $timestamp = $timestamp-(60*60*4);
        $conn = r\connect('ilscomms.ilsny.com');
        $result = r\db("VSM2")->table("Details")->filter(r\row('DvrID')->eq($dvr)->rAnd(r\row('TimeStamp')->gt($timestamp)))->run($conn);
        $elements = $result->toArray();
        return $elements;
    }
    
    function getAllLogs($dvr){
        $conn = r\connect('ilscomms.ilsny.com');
        $result = r\db("VSM2")->table("Details")->filter(r\row('DvrID')->eq($dvr))->run($conn);
        $rdata = $result->toArray();
        return $rdata;
    }
    
    
    
    
}
