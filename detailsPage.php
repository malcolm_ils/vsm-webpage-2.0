<?PHP
session_start();


if (empty($_SESSION['USERGUID']) || empty($_SESSION['NAME'])) {

    echo "No Access";
    die();
}

$client = $_GET['client'];
$site = $_GET['site'];
$dvr = $_GET['dvr'];

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>VSM 2.0 - DVR Details</title>


        <!-- Style Sheets - Internal -->
        <link href="css/forms.css" rel="stylesheet" type="text/css"/>
        <link href="css/tables.css" rel="stylesheet" type="text/css"/>
        <!-- END Style Sheets - Internal -->


        <!-- External Style Sheets -->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <!-- End External Style Sheets -->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> 
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <style>

            html{
                /*overflow: hidden;*/
            }

            body{
                /*overflow: hidden;*/
            }

            .lockdown {
                background-color: #aec6cf !important;
            }
            .issue {
                background-color: #fdfd96 !important;
            }

            .error {
                background-color: #fdfd96 !important;
            }


            .good {
                background-color: #77dd77 !important;
            }


            .failed {
                background-color: #ff6961 !important;
            }

            .spinner {
                margin: 100px auto;
                width: 40px;
                height: 40px;
                position: relative;
                text-align: center;

                -webkit-animation: sk-rotate 2.0s infinite linear;
                animation: sk-rotate 2.0s infinite linear;
            }

            .dot1, .dot2 {
                width: 60%;
                height: 60%;
                display: inline-block;
                position: absolute;
                top: 0;
                background-color: #333;
                border-radius: 100%;

                -webkit-animation: sk-bounce 2.0s infinite ease-in-out;
                animation: sk-bounce 2.0s infinite ease-in-out;
            }

            .dot2 {
                top: auto;
                bottom: 0;
                -webkit-animation-delay: -1.0s;
                animation-delay: -1.0s;
            }
            
            .pageHeader {
                width: 100%;
                text-align: center;
                font-weight: bold;
                font-size: x-large;
                font-variant-caps:small-caps;
                font-family: Lucida Console;
            }

            @-webkit-keyframes sk-rotate { 100% { -webkit-transform: rotate(360deg) }}
            @keyframes sk-rotate { 100% { transform: rotate(360deg); -webkit-transform: rotate(360deg) }}

            @-webkit-keyframes sk-bounce {
                0%, 100% { -webkit-transform: scale(0.0) }
                50% { -webkit-transform: scale(1.0) }
            }

            @keyframes sk-bounce {
                0%, 100% { 
                    transform: scale(0.0);
                    -webkit-transform: scale(0.0);
                } 50% { 
                    transform: scale(1.0);
                    -webkit-transform: scale(1.0);
                }
            }
        </style>
    </head>

    <body>
        <div>
            <h3 class="pageHeader">VSM Details</h3>
        </div>
        <h3 class="table-Title">Current Machine Status</h3>
        <div id="SystemDetails">
            <div class="spinner">
                <div class="dot1"></div>
                <div class="dot2"></div>
            </div>
        </div>


    </body>
    <script>
        $(document).ready(function () {              
            $.post("dataDetails.php", {client: "<?php echo $client ?>", site: "<?php echo $site ?>", dvr: "<?php echo $dvr ?>"})
                    .done(function (data) {
                        $("#SystemDetails").html(data);
                        var element = $("#td-currentStatus");
                        var status = element.html();
                        switch(status){
                                case "OK":
                                    element.addClass('good');
                                    break;
                                case "LOCKDOWN":
                                    element.addClass('lockdown');
                                    break;
                                case "Failed":
                                   element.addClass('failed');
                                    break;
                                case "DVR Software Not Running":
                                    element.addClass('failed');
                                    break;
                                case "ISSUE":
                                    element.addClass('issue');
                                    break;
                                case "VSM Starting":
                                    element.addClass('good');
                                    break;
                                case "Lost Cameras":
                                    element.addClass('issue');
                                    break;
                                default:
                                    element.addClass('issue');
                        }
                    });
        });

    </script>
    
    
    
    
</html> 