<?PHP
session_start();


if (empty($_SESSION['USERGUID']) || empty($_SESSION['NAME'])) {

    echo "No Access";
    die();
}

include_once "data.class.php";
$data = new vsm20data();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>VSM 2.0</title>


        <!-- Style Sheets - Internal -->
        <link href="css/forms.css" rel="stylesheet" type="text/css"/>
        <!-- END Style Sheets - Internal -->


        <!-- External Style Sheets -->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <!-- End External Style Sheets -->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> 
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <style>

            html{
                overflow: hidden;
            }

            body{
                overflow: hidden;
            }

            .lockdown {
                background-color: #aec6cf !important;
            }
            .issue {
                background-color: #fdfd96 !important;
            }

            .error {
                background-color: #fdfd96 !important;
            }


            .good {
                background-color: #77dd77 !important;
            }


            .failed {
                background-color: #ff6961 !important;
            }

            .spinner {
                margin: 100px auto;
                width: 40px;
                height: 40px;
                position: relative;
                text-align: center;

                -webkit-animation: sk-rotate 2.0s infinite linear;
                animation: sk-rotate 2.0s infinite linear;
            }

            .dot1, .dot2 {
                width: 60%;
                height: 60%;
                display: inline-block;
                position: absolute;
                top: 0;
                background-color: #333;
                border-radius: 100%;

                -webkit-animation: sk-bounce 2.0s infinite ease-in-out;
                animation: sk-bounce 2.0s infinite ease-in-out;
            }

            .dot2 {
                top: auto;
                bottom: 0;
                -webkit-animation-delay: -1.0s;
                animation-delay: -1.0s;
            }
            
            
            #pageTitle{
                text-align: center;
                font-weight: bold;
                font-size:20px;
                padding:0;
                margin:0;
            }
            #optionsBar{
    position: relative;
    padding: 5px;
    margin-bottom: 10px;
}
#ToggleButtons{
    position: relative;
    marign:0;
}
#addItem{
    position: absolute;
    right: 10px;
    top: 15px;
}
#toggleTitle{
    padding: 0;
    margin: 0;
    font-weight: bold;
    font-size: 15px;
}



            @-webkit-keyframes sk-rotate { 100% { -webkit-transform: rotate(360deg) }}
            @keyframes sk-rotate { 100% { transform: rotate(360deg); -webkit-transform: rotate(360deg) }}

            @-webkit-keyframes sk-bounce {
                0%, 100% { -webkit-transform: scale(0.0) }
                50% { -webkit-transform: scale(1.0) }
            }

            @keyframes sk-bounce {
                0%, 100% { 
                    transform: scale(0.0);
                    -webkit-transform: scale(0.0);
                } 50% { 
                    transform: scale(1.0);
                    -webkit-transform: scale(1.0);
                }
            }
        </style>
    </head>

    <body>
        <div id="pageTitle"><p>Vital Sign Monitoring <span>2.0</span></p></div>
        
        
        <div id="optionsBar">
        

            <div id="ToggleButtons">
                <p id="toggleTitle">Show Columns</p>
                <input type="checkbox" class="toggleVisibility" data-column="3"  checked>Client</input>
                <input type="checkbox" class="toggleVisibility"  data-column="4" checked>Site</input>
                <input type="checkbox" class="toggleVisibility"  data-column="5" checked>Description</input>
                <input type="checkbox" class="toggleVisibility"  data-column="6" checked>IP</input>
                <input type="checkbox" class="toggleVisibility"  data-column="7" checked>Interval</input>
                <input type="checkbox" class="toggleVisibility"  data-column="10" checked>Version</input>
            </div>
            
            <div id="addItem">
                <button>
                New <i class="fas fa-plus"></i> 
                </button>
            </div>    
        </div>
        
        <table id="example" class="display" style="width:100%">
            <thead>
                <tr>
                    <th>C</th>
                    <th>s</th>
                    <th>d</th>
                    <th>Client</th>
                    <th>Site</th>
                    <th>Description</th>
                    <th>IP</th>
                    <th>Interval</th>
                    <th>Last Checkin</th>
                    <th>Status</th>
                    <th>Version</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
            <tfoot>
                <tr>
                    <th>C</th>
                    <th>s</th>
                    <th>d</th>
                    <th>Client</th>
                    <th>Site</th>
                    <th>Description</th>
                    <th>IP</th>
                    <th>Interval</th>
                    <th>Last Checkin</th>
                    <th>Status</th>
                    <th>Version</th>
                </tr>
            </tfoot>
        </table>
        
        <!-- **START** Log Dialog -->
        <div style="display:none;" id="dialog-message" title="Download complete">
            <div id="logdata"><div class="spinner">
                    <div class="dot1"></div>
                    <div class="dot2"></div>
                </div></div>
        </div>
        <!-- **END** LOG Dialog -->
        
        
        <!--       ADD ITEM dialog -->
        <div style="display:none;" id="dialog-AddItem">
            <div id="userInput">
                <form id="form-additem">
                    <div class="form-group"> 
                        <label id="label-client">Client:</label>
                        <select id="select-Clients">
                           <?php
                                echo $data ->GetClients();
                           ?>
                        </select>

                    </div>
                    <div class="form-group">
                        <label id="label-Site">Site:</label>
                        <select id="select-Sites">
                            
                        </select>
                    </div>
                    <div class="form-group">
                        <label id="label-DVR">DVR #:</label>
                        <select id="select-DVRs">
                            
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="ServerIndex" id="label-ServerIndex">Server Index:</label>
                        <select id="ServerIndex">
                            <option value="0">1</option>           
                            <option value="1">2</option>
                            <option value="2">3</option> 
                            <option value="3">4</option> 
                        </select>
                    </div>
                    <div class="form-group">
                        <label id="label-Description">Desc: </label>
                        <input type="text" id="Description" disabled></input>
                    </div>
                    <div class="form-group">
                        <label for="Type" id="label-Type">Server Type:</label>
                        <select id="Type" disabled>
                            <option value ="1">DVR</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="Vendor" id="label-Vendor">Vendor:</label>
                        <select id="Vendor" disabled>
                            <option value="1">Axxon</option>
<!--                            <option value="2">Geovision</option>-->
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="IP" id="label-IP">IP Address:</label>
                        <input id="IP" type="text" value="" disabled>
                    </div>
                    <div class="form-group">
                        <label for="Port" id="label-ip">Port:</label>
                        <input id="Port" type="number" min="0" value="" disabled>
                    </div>
                    <div class="form-group">
                        <label for="Interval" id="label-Interval">Checkin Interval (minutes)</label>
                        <input id="Interval" type="number" min="10" step="5" max="60" value="15">
                    </div>
                </form>
            </div>
        </div>
        <!--    ** END **   ADD ITEM dialog -->
        
        <!-- **START** EDIT ITEM dialog -->
        <div style="display:none;" id="dialog-EditItem">
            <div id="edit-client"></div>
            <div id="edit-site"></div>
            <div id="edit-dvr"></div>
            <div id="edit-serverIndex"></div>
            <div id="edit-desc"></div>
            <div id="edit-serverType"></div>
            <div id="edit-vendor"></div>
            
            <div id="userInput">
                <form id="form-edititem">
                    <div class="form-group">
                        <label for="ServerIndex" id="label-ServerIndex">Server Index:</label>
                        <select id="edit-ServerIndex">
                            <option value="0">1</option>           
                            <option value="1">2</option>
                            <option value="2">3</option> 
                            <option value="3">4</option>           
                        </select> 
                    </div>
                    <div class="form-group">
                        <label for="Vendor" id="label-Vendor">Vendor:</label>
                        <select id="edit-Vendor">
                            <option value="1">Axxon</option>           
                        </select> 
                    </div>
                    <div class="form-group">
                        <label for="IP" id="label-IP">IP Address:</label>
                        <input id="edit-IP" type="text" value="">
                    </div>
                    <div class="form-group">
                        <label for="Port" id="label-ip">Port:</label>
                        <input id="edit-Port" type="number" min="0" value="">
                    </div>
                    <div class="form-group">
                        <label for="Interval" id="label-Interval">Checkin Interval (minutes)</label>
                        <input id="edit-Interval" type="number" min="0" step="5" max="60" value="">
                    </div>
                </form>
            </div>
        </div>
        <!-- **END** EDIT ITEM dialog -->

        
        <div style="display:none;" id="loading">
            <div class="spinner">
                    <div class="dot1"></div>
                    <div class="dot2"></div>
            </div>
        </div>
    </body>
    <script>
        $(document).ready(function () {
            var table = $('#example').DataTable({
                scrollY: '85vh',
                scrollCollapse: true,
                paging: false,
                "orderClasses": false,
                "ajax": "data.php",
                "columnDefs": [
                    {"targets": [0], "visible": false, "searchable": false},
                    {"targets": [1], "visible": false, "searchable": false},
                    {"targets": [2], "visible": false, "searchable": false},
                ],
                "createdRow": function (row, data, dataIndex) {

                    $(row).attr('data-client', data[0]);
                    $(row).attr('data-site', data[1]);
                    $(row).attr('data-dvr', data[2]);
                    $(row).attr('data-name', data[3] + " - " + data[5]);

                    switch(data[9]){
                        case "OK":
                            $(row).addClass('good');
                            break;
                        case "LOCKDOWN":
                            $(row).addClass('lockdown');
                            break;
                        case "Failed":
                            $(row).addClass('failed');
                            break;
                        case "Missed Beat":
                            $(row).addClass('failed');
                            break;
                        case "DVR Software Not Running":
                            $(row).addClass('failed');
                            break;
                        case "ISSUE":
                            $(row).addClass('issue');
                            break;
                        case "VSM Starting":
                            $(row).addClass('good');
                            break;
                        case "Lost Cameras":
                            $(row).addClass('issue');
                            break;
                        default:
                            $(row).addClass('issue');
                    }
                }
            });

            setInterval(function () {

                $.post("TagOld.php");

                table.ajax.reload(null, false);

            }, 5000);

        $('.toggleVisibility').on( 'click', function (e) {
            
           var column = table.column( $(this).attr('data-column') );
       
                 column.visible( ! column.visible() );
            } );

            $('#example tbody').on('click', 'tr', function () {
                var client = $(this).data("client");
                var site = $(this).data("site");
                var dvr = $(this).data("dvr");
                $("#logdata").html($("#loading").html());

                $.post("log.php", {client: $(this).data("client"), site: $(this).data("site"), dvr: $(this).data("dvr")})
                        .done(function (data) {
                            $("#logdata").html(data);
                        });
                var data2 = $(this).data("name");
                $("#dialog-message").dialog({
                    modal: true,
                    height: 600,
                    width: 550,
                    title: "" + $(this).data("name"),
                    buttons: {
                        Ok: function () {
                            $(this).dialog("close");
                        },
                        Details: function(){
                            window.open("detailsPage.php?client=" + client
                                    +"&site="+site+"&dvr="+dvr, '_blank');
                        },
                        Edit: function(){
                            launchEditDialog(data2,client, site, dvr);
                        }
                    }
                });
            });
           
           $('#addItem').on('click', function () {
                $("#dialog-AddItem").dialog({
                    
                    modal: true,
                    height: 600,
                    width: 550,
                    title: "Add Item",
                    buttons: {
                        Submit: function(){
                            SubmitItem();
                        },
                        Cancel: function () {
                            $(this).dialog("close");
                        }
                    }
                });
            });
            
            $('#select-Clients').change(function(){
                var client = $('#select-Clients').val();
               
               $('#select-Sites').load("getSites.php?client="+client);
               
            });
            
            $('#select-Sites').change(function(){
                $.get( "getDVR.php", { client: $('#select-Clients option:selected').text(), site: $('#select-Sites option:selected').text() } )
                    .done(function( data ) {
                        $("#select-DVRs").html(data);
                    });
            });
            
            $('#select-DVRs').change(function(){
                $.get( "getDVR_Details.php", { dvr: $("#select-DVRs").val() } )
                    .done(function( data ) {
                       data = JSON.parse(data);
                       $("#Description").val(data[0]);
                       $("#IP").val(data[1]);
                       $("#Port").val(data[2]);
                    });
            });
            
            function SubmitItem(){
                var client  = $('#select-Clients').val();
                var site = $("#select-Sites").val();
                var dvr = $('#select-DVRs').val();
                var serverIndex = $('#select-serverIndex').val();
                var descr = $('#Description').val();
                var ip = $('#IP').val();
                var port = $('#Port').val();
                var interval = $('#Interval').val();
                var vendor = $('#Vendor').val();

                $.get( " submitItem.php", {client: client, site: site, dvr: dvr, serverIndex: serverIndex, descr: descr, ip: ip, port: port, interval: interval, vendor: vendor})
                        .done(function(data){
                            alert(data["status"]);
                        });
            }

            function launchEditDialog(title, client, site, dvr){
            $.get( "getConfig.php", { client: client, site: site, dvr: dvr } )
                    .done(function( data ) {
                    
                        var interval = data["Interval"];
                        var temp = interval.split(":");
                        interval = temp[1];


                        $("#edit-ServerIndex").val(data["ServerIndex"]);
                        $("#edit-Vendor").val(data["Vendor"]);
                        $("#edit-IP").val(data["IP"]);
                        $("#edit-Port").val(data["Port"]);
                        $("#edit-Interval").val(interval);
                    });
                                $("#dialog-EditItem").dialog({
                    modal: true,
                    height: 600,
                    width: 550,
                    title: "Edit "+ title,
                    buttons: {
                        Cancel: function () {
                            $(this).dialog("close");
                        },
                        Submit: function(){
                            
                            var ip = $('#edit-IP').val();
                            var port = $('#edit-Port').val();
                            var interval = $('#edit-Interval').val();
                            var vendor = $('#edit-Vendor').val();
                            var serverIndex = $("#edit-ServerIndex").val();
                            editConfig(client, site, dvr, serverIndex, ip, port, interval, vendor );
                        }
                    }
            });
            
        }
        
        function editConfig(client, site, dvr, serverIndex, ip, port, interval, vendor){
                $.get( "editConfig.php", {client: client, site: site, dvr: dvr, serverIndex: serverIndex, ip: ip, port: port, interval: interval, vendor: vendor} )
                    .done(function( data ) {
                        alert("Config Updated");
                        $("#dialog-EditItem").dialog("close");
            });
        }
        
            
                
    });
    </script>
</html> 