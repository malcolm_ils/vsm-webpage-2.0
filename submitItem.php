<?php
$client = (int)$_GET['client'];
$site = (int)$_GET['site'];
$dvr = (int)$_GET['dvr'];
$description =(string)$_GET['descr'];
$serverIndex = (int)$_GET['serverIndex'];
$ip = (string)$_GET['ip'];
$port = (int)$_GET['port'];
$vendor = (int)$_GET['vendor'];
$interval = $_GET['interval'];


$interval = "00:".$interval.":00";

require_once("rdb/rdb.php");
    header('Content-Type: application/json');
    $conn = r\connect('ilscomms.ilsny.com');
        
    (array)$result = r\db("VSM2")->table("Config")->filter(array("Client"=> $client,"Site" => $site,"DvrID" => $dvr))->run($conn);
     $rdata = $result->toArray();

   if(count($rdata) > 0 ){
        echo json_encode(array( "status" => "DVR Already Configured"));
    }else{
        $insertObj = array("Client"=> $client,"Site" => $site,"DvrID" => $dvr, "ServerIndex"=>$serverIndex, "Description"=>$description, 
            "IP"=>$ip, "Port"=>$port, "Interval"=>$interval, "Vendor"=>$vendor);
         $result = r\db("VSM2")->table("Config")->insert($insertObj)->run($conn);
      echo json_encode(array( "status" => "Config Inserted"));
    }
  
    ?>