<?PHP

    require_once("rdb/rdb.php");
    include "data.class.php";
    $commsData = new vsm20data();
    
    $client = (int)$_POST['client'];
    $site = (int)$_POST['site'];
    $dvr = (int)$_POST['dvr'];
    
    $conn = r\connect('ilscomms.ilsny.com');
    
    
    /***********************CheckIn Db Connec - Start*************************/
    $allclient = $commsData->GetAllClients();
    $result1 = r\db("VSM2")->table("CheckIn")->filter(array("Client"=> $client,"Site" => $site,"DvrID" => $dvr))->run($conn);
    $rdata1 = $result1 ->toArray();
    $rdata1 = (array)$rdata1[0];
    
    /***********************Checkin DB connect  -End*************************/
    
    /***********************PAGE HEADER -Start*************************/
    
    $pageHeaderHTML = 
            "<div><table id='table-PageHeader'>"
           . "<tr class='detailsLabels' id='pageHeaderLabels'><td>Status</td><td>Client</td><td>Site</td><td>Description</td><td>Last Checkin</td><td>IP</td><td>Version</td></tr>";
    $pageHeaderHTML .= "<tr><td id='td-currentStatus'>".$rdata1["Status"]."</td><td>".$allclient[$rdata1["Client"]]."</td><td>".$allclient[$rdata1["Site"]]."</td>"
            . "<td>".$rdata1["Description"]."</td><td>".$rdata1["Time"]."</td><td>".$rdata1["IP"]."</td><td>".$rdata1["Version"]."</td></tr>";
        
        $pageHeaderHTML .="</table></div>";

        /***********************PAGE HEADER -End*************************/
        
        
        
    /************SYSTEM DETAILS DB Conn - START ******************/
    
    $result = r\db("VSM2")->table("SystemDetails")->filter(array("Client"=> $client,"Site" => $site,"DvrID" => $dvr))->
            pluck(array("NetworkDetails","DriveCount", "DriveDetails"))->run($conn);
    
   $element  = $result->toArray();
   $element = (array)$element[0];
   /************SYSTEM DETAILS DB Conn - END ******************/
   
   /************NETWORK TABLE - START ******************/
   $NetworkDetails = (array)$element["NetworkDetails"];

   $networkHTML = "<div id='div-Network'><p class='table-Title'>Network</p>"
            . "<table id='table-Network'>"
            . "<tr class='detailsLabels' id='networkLabels'><th>Name</th><th>Mac Address</th><th>IP</th><th>DNS</th></tr>";
 
   foreach($NetworkDetails as $Adapter){
    $DnsList =(array) $Adapter["DNS"];
    $IPs = $Adapter["IP Address"];
    $Macs = $Adapter["Mac Address"];
    $AdapterName = $Adapter["Name"];
    $AdapterType = $Adapter["Type"];
    
//        $DnsList =(array) $NetworkDetails["DNS"];
//    $IPs = $NetworkDetails["IP Address"];
//    $Macs = $NetworkDetails["Mac Address"];
//    $AdapterName = $NetworkDetails["Name"];
//    $AdapterType = $NetworkDetails["Type"];


    $networkHTML .= "<tr><td>".$AdapterName."</td><td>".$Macs."</td><td>".$IPs."</td><td>";

$networkHTML .= $DnsList[0]." / ".$DnsList[1];
//$networkHTML .= $DnsList;
//    foreach($DnsList as $dns){
//        $count++;
//        if($count > 1 )
//            $networkHTML .=" / ";
//        $networkHTML .= $dns;
//    }

    $networkHTML .="</td></tr>";
   }
   
   //print_r($IPs);
   $networkHTML .= "</table></div>";
   
   /*********NETWORK TABLE CLOSE************************/
   
   
  /**********STORAGE TABLE - START ********************/ 
  $storageData = "<div id='div-Storage'><p class='table-Title'>Storage</p>"
          ."<table id='table-Storage'>"
          . "<tr class='detailsLabels' id='StorageLabels'><th>Letter(Name)</th><th>Freespace</th><th>Capacity</th></tr>"; 
   
   
   $driveCount = $element['DriveCount'];
   $driveDetails = (array)$element['DriveDetails'];
   foreach($driveDetails as $drive){       
       
       if($drive["capacity"]>0){
           $percentFree = floor(($drive["freeSpace"]/$drive["capacity"])*100);
        $storageData .="<tr><td>".$drive['name']."</td><td>".$drive["freeSpace"]." (".$percentFree."%)</td><td> ".$drive["capacity"]."</td></tr>";
       
       //print_r("Name ".$drive['name']." Capacity: ".$drive["capacity"]." Free Space: ".$drive["freeSpace"]);
       }
   }
  $storageData .= "</table></div>";
  /*************STORAGE TABLE - CLOSE**************************/
  
  /*************LOG TABLE - START**************************/
  $date = getDate();
  $today = $date["mday"];
  $month = $date["month"];
  $year = $date["year"];
  $timestamp = strtotime("".$today." ".$month." ".$year."");  
  $timestamp = $timestamp-(60*60*4);
  
  $result3 = r\db("VSM2")->table("Details")->filter(r\row('DvrID')->eq($dvr)->rAnd(r\row('TimeStamp')->gt($timestamp)))->run($conn);
    $elements3 = $result3->toArray();
    
    $logTableHTML = "<div id='div-Logs'><p class='table-Title'>Logs</p><table id='table-Logs'>"
           . "<tr class='detailsLabels' id='logLabels'><th>Time</th><th>Status</th><th>Cameras On</th><th>SignalLoss</th><th>Cameras Off</th></tr>";

    $logCount = 0;
    foreach($elements3 as $log){
        
        
        $offCount = 0;
        $onCount = 0;
        $lossCount = 0;
        $camOff = (array)$log["CamerasOff"];
        $camOn = (array)$log["CamerasOn"];
        $camLoss = (array)$log["SignalLoss"];
        
        $logCount++;

        
        $dataLog[strtotime($log['Time'])] .= "<tr><td>".$log['Time']."</td>";
        $dataLog[strtotime($log['Time'])] .= "<td>".$log['DetailedStatus']."</td>";
        

        
        
///////////// CAMERAS ON
        if(count($camOn) > 0){
            $dataLog[strtotime($log['Time'])] .= "<td>";
            foreach($camOn as $on){
                
                $onCount++;
                if($onCount > 1){
                    $dataLog[strtotime($log['Time'])] .= ", ";
                }
                $dataLog[strtotime($log['Time'])] .= $on;
               // print_r($onCount." -- ".$on." --");
            }
            $dataLog[strtotime($log['Time'])] .= "</td>";
        } else {
            $datalog[strtotime($log['Time'])] .= "<td> </td>";
        }
        
        
        
//        ////////////// Signal Loss
        if(count($camLoss) > 0){
             $dataLog[strtotime($log['Time'])] .= "<td>";
            foreach($camLoss as $loss){
                $lossCount++;
                if($lossCount > 1){
                    $dataLog[strtotime($log['Time'])] .= ", ";
                }
                $dataLog[strtotime($log['Time'])] .= "".$loss."";
            }
            $dataLog[strtotime($log['Time'])] .= "</td>";
        } else {
            $dataLog[strtotime($log['Time'])] .= "<td> </td>";
        }
 

    
    ///////////// CAMERAS OFF
        if(count($camOff) > 0){
            $dataLog[strtotime($log['Time'])] .= "<td>";
            foreach($camOff as $off){
                $offCount++;
                if($offCount > 1){
                    $dataLog[strtotime($log['Time'])] .= ", ";
                }
                $dataLog[strtotime($log['Time'])] .= "".$off."";
            }
            $dataLog[strtotime($log['Time'])] .= "</td>";
        } else {
            $dataLog[strtotime($log['Time'])] .= "<td> </td>";
        }
                $datalog[strtotime($log['Time'])] .= "</tr>";
    }
    
    
    
    ksort($dataLog);
    $reverse = array_reverse($dataLog);
     
    foreach($reverse as $val){
        $logTableHTML .= "".$val;
 
    }
  
    
    
 $logTableHTML .= "</table></div>";
  
  /*************LOG TABLE - CLOSE**************************/

  $rdata .= $pageHeaderHTML;
  $rdata .= $networkHTML;
  $rdata .= $storageData;
  $rdata .= $logTableHTML;

  echo $rdata;

?>